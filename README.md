# Digital Analytics Program

Embeds the appropriate Google Analytics javascript file for US federal
agencies. No library needs to be downloaded, the file being used
is on [page](https://analytics.usa.gov).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/usfedgov_google_analytics).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/usfedgov_google_analytics).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Configure the desired settings at Administration >
   Configuration > Web Services > Digital Analytics Program.


## Maintainers

- James Glasgow - [jrglasgow](https://www.drupal.org/u/jrglasgow)
- lcatlett - [lcatlett](https://www.drupal.org/u/lcatlett)
- Luke Meier - [Lukey](https://www.drupal.org/u/lukey)
- Heather Choi - [heatherchoi@hotmail.com](hhttps://www.drupal.org/u/heatherchoihotmailcom)
- orbmantell - [orbmantell](https://www.drupal.org/u/orbmantell)
- David Cameron - [dcam](https://www.drupal.org/u/dcam)
