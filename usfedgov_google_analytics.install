<?php

/**
 * @file
 * Install, update, and requirements hooks for usfedgov_google_analytics.
 */

declare(strict_types=1);

use Drupal\Core\Hook\Attribute\LegacyHook;
use Drupal\usfedgov_google_analytics\Hook\RuntimeRequirements;

/**
 * Implements hook_requirements().
 */
#[LegacyHook]
function usfedgov_google_analytics_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    $requirements = \Drupal::service(RuntimeRequirements::class)->checkAgencySetting();
  }
  return $requirements;
}

/**
 * Correct the value of boolean settings.
 */
function usfedgov_google_analytics_update_20002() {
  $config = \Drupal::configFactory()->getEditable('usfedgov_google_analytics.settings');
  $config->set('usfedgov_google_analytics__load_from_cdn', (bool) $config->get('usfedgov_google_analytics__load_from_cdn'));
  $config->set('usfedgov_google_analytics__settings.extra.autotracker', (bool) $config->get('usfedgov_google_analytics__settings.extra.autotracker'));
  $config->set('usfedgov_google_analytics__settings.extra.parallelcd', (bool) $config->get('usfedgov_google_analytics__settings.extra.parallelcd'));
  $config->save();
}

/**
 * Change the cto setting value to be numeric.
 */
function usfedgov_google_analytics_update_20003() {
  $config = \Drupal::configFactory()->getEditable('usfedgov_google_analytics.settings');
  $cto = $config->get('usfedgov_google_analytics__settings.extra.cto');
  $cto = is_numeric($cto) ? (int) $cto : 24;
  $config->set('usfedgov_google_analytics__settings.extra.cto', $cto);
  $config->save();
}

/**
 * Update configuration to the version 3 schema.
 */
function usfedgov_google_analytics_update_30000() {
  $config = \Drupal::configFactory()->getEditable('usfedgov_google_analytics.settings');

  $config->set('status', !$config->get('usfedgov_google_analytics__disable'));
  $config->clear('usfedgov_google_analytics__disable');

  $load_cdn = $config->get('usfedgov_google_analytics__load_from_cdn');
  $load_min = $config->get('usfedgov_google_analytics__load_minified');
  $config->set('library', $load_cdn ? 'cdn' : ($load_min ? 'local_minified.8.0.0' : 'local.8.0.0'));
  $config->clear('usfedgov_google_analytics__load_from_cdn');
  $config->clear('usfedgov_google_analytics__load_minified');

  $config->set('query_parameters.agency', $config->get('usfedgov_google_analytics__settings.agency'));
  $config->set('query_parameters.subagency', $config->get('usfedgov_google_analytics__settings.subagency'));
  $config->set('query_parameters.sitetopic', '');
  $config->set('query_parameters.siteplatform', 'Drupal');
  $config->set('query_parameters.sp', $config->get('usfedgov_google_analytics__settings.extra.sp'));
  $config->set('query_parameters.exts', $config->get('usfedgov_google_analytics__settings.extra.exts'));
  $config->set('query_parameters.yt', $config->get('usfedgov_google_analytics__settings.extra.yt'));
  $config->set('query_parameters.htmlvideo', TRUE);
  $config->set('query_parameters.ytm', 25);
  $config->set('query_parameters.sdor', '');
  $config->set('query_parameters.autotracker', $config->get('usfedgov_google_analytics__settings.extra.autotracker'));
  $config->set('query_parameters.cto', $config->get('usfedgov_google_analytics__settings.extra.cto'));
  $config->set('query_parameters.pga4', $config->get('usfedgov_google_analytics__settings.extra.pga4'));
  $config->set('query_parameters.parallelcd', $config->get('usfedgov_google_analytics__settings.extra.parallelcd'));

  $current_palagencydim = $config->get('usfedgov_google_analytics__settings.extra.palagencydim');
  $palagencydim = is_numeric($current_palagencydim) ? (int) $current_palagencydim : 1;
  $config->set('query_parameters.palagencydim', $palagencydim);

  $current_palsubagencydim = $config->get('usfedgov_google_analytics__settings.extra.palsubagencydim');
  $palsubagencydim = is_numeric($current_palsubagencydim) ? (int) $current_palsubagencydim : 2;
  $config->set('query_parameters.palsubagencydim', $palsubagencydim);

  $current_palversiondim = $config->get('usfedgov_google_analytics__settings.extra.palversiondim');
  $palversiondim = is_numeric($current_palversiondim) ? (int) $current_palversiondim : 3;
  $config->set('query_parameters.palversiondim', $palversiondim);

  $config->set('query_parameters.paltopicdim', 4);
  $config->set('query_parameters.palplatformdim', 5);
  $config->set('query_parameters.palscriptsrcdim', 6);
  $config->set('query_parameters.palurlprotocoldim', 7);
  $config->set('query_parameters.palinteractiontypedim', 8);
  $config->set('query_parameters.dapdev', FALSE);
  $config->clear('usfedgov_google_analytics__settings');

  $config->save();
}
