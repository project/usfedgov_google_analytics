<?php

namespace Drupal\usfedgov_google_analytics\Form;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the usfedgov_google_analytics settings form.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * The options for the ytm parameter.
   */
  const YTM_OPTIONS = [10, 20, 25];

  /**
   * Common settings for custom dimension form elements.
   */
  const CUSTOM_DIMENSION_SLOT_SETTINGS = [
    '#min' => 1,
    '#max' => 425,
    '#step' => 1,
  ];

  /**
   * The library.discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $settings_form = new static(
      $container->get('config.factory'),
      $container->get('config.typed')
    );
    $settings_form->setLibraryDiscovery($container->get('library.discovery'));
    return $settings_form;
  }

  /**
   * Sets the libraryDiscovery property.
   *
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   *   The library.discovery service.
   *
   * @return \Drupal\usfedgov_google_analytics\Form\SettingsForm
   *   The form object.
   */
  public function setLibraryDiscovery(LibraryDiscoveryInterface $library_discovery): self {
    $this->libraryDiscovery = $library_discovery;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'usfedgov_google_analytics_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['usfedgov_google_analytics.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#config_target' => 'usfedgov_google_analytics.settings:status',
    ];
    $form['library'] = [
      '#type' => 'radios',
      '#title' => $this->t('Choose how to serve the DAP JavaScript'),
      '#options' => [
        'cdn' => $this->t('Latest version from the DAP CDN (recommended)'),
        'local_minified.8.6.0' => $this->t('Version 8.6.0 from local files, minified'),
        'local.8.6.0' => $this->t('Version 8.6.0 from local files, not minified'),
        'local_minified.8.5.0' => $this->t('Version 8.5.0 from local files, minified'),
        'local.8.5.0' => $this->t('Version 8.5.0 from local files, not minified'),
        'local_minified.8.0.0' => $this->t('Version 8.0.0 from local files, minified'),
        'local.8.0.0' => $this->t('Version 8.0.0 from local files, not minified'),
      ],
      '#config_target' => 'usfedgov_google_analytics.settings:library',
    ];
    $form['more_info'] = [
      '#markup' => '<h2>' . $this->t('Query parameter settings') . '</h2><p>' . $this->t('More information about the settings can be found in the <a href=":url">DAP Quick Technical Guide</a>.', [
        ':url' => 'https://github.com/digital-analytics-program/gov-wide-code/wiki/DAP-Quick-Technical-Guide',
      ]) . '</p>',
    ];

    $form['agency'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Agency'),
      '#description' => $this->t('The main Federal agency which this website is part of. i.e. DHS'),
      '#required' => TRUE,
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.agency',
    ];
    $form['subagency'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sub-Agency'),
      '#description' => $this->t('The sub-agency, if any, which this website is part of. i.e. FEMA'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.subagency',
    ];
    $form['sitetopic'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site topic'),
      '#description' => $this->t('This enables the DAP team to understand differences in web trends across different website topics, such as health or travel.'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.sitetopic',
    ];
    $form['siteplatform'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site platform'),
      '#description' => $this->t('This enables the DAP team to understand differences in web trends across different website platforms, such as Drupal.'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.siteplatform',
    ];
    $form['sp'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional Search Parameters'),
      '#description' => $this->t('By default, these search parameters are automatically tracked:<br>@parameter_list', [
        '@parameter_list' => 'q, query, nasaInclude, k, querytext, keys, qt, search_input, search, globalSearch,  goog, s, gsearch, search_keywords,  SearchableText, sp_q,  qs, psnetsearch, locate, lookup, search_api_views_fulltext, keywords, request, _3_keywords, searchString',
      ]),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.sp',
    ];
    $form['download_tracking'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Download tracking'),
    ];
    $form['download_tracking']['autotracker'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable download tracking'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.autotracker',
    ];
    $form['download_tracking']['exts'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional File Extensions'),
      '#description' => $this->t('By default, the DAP Code automatically tracks downloads for these file types:<br>@extension_list', [
        '@extension_list' => '7z, avi, csv, doc, docx, dwfx, dwg, dwgd, dxf, epub, exe, gz, jpg, js, mobi, mov, mp3, mp4, msi, pdf, ppt, pptx, rar, rfa, rvt, swf, tgz, txt, wma, wmv, xls, xlsm, xlsx, zip',
      ]),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.exts',
    ];
    $form['video_tracking'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Video tracking'),
    ];
    $form['video_tracking']['yt'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable YouTube video tracking'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.yt',
    ];
    $form['video_tracking']['htmlvideo'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable HTML5 Media tracking'),
      '#description' => $this->t('HTML5 video tracking requires at least DAP version 8.3.0.'),
      '#states' => [
        'disabled' => [
          ':input[name="library"]' => [
            ['value' => 'local_minified.8.0.0'],
            'or',
            ['value' => 'local.8.0.0'],
          ],
        ],
      ],
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.htmlvideo',
    ];
    $form['video_tracking']['ytm'] = [
      '#type' => 'radios',
      '#title' => $this->t('YouTube video / HTML5 Media Milestones'),
      '#description' => $this->t('If YouTube video or HTML5 Media tracking is enabled, then the DAP code will track video progress every 10%, 20%, or 25% (default) of the video duration.'),
      '#options' => array_combine(self::YTM_OPTIONS, array_map(function ($value) {
        return "$value%";
      }, self::YTM_OPTIONS)),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.ytm',
    ];
    $form['sdor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sub-domain Tracking'),
      '#description' => $this->t('By default, sub-domains on the same domain will be treated as separate sites.<br>If set to custom value, for example "sub.domain.com" or "site.com", all sub-domains of the current domain will be linked and links between these portals will be considered internal links.'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.sdor',
    ];
    $form['cto'] = [
      '#type' => 'number',
      '#title' => $this->t('Cookie expiration length (in months)'),
      '#description' => $this->t('Note: Chrome caps the expiration length of cookies at 400 days.'),
      '#step' => 1,
      '#min' => 0,
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.cto',
    ];

    $form['parallel_tracking'] = [
      '#type' => 'details',
      '#title' => $this->t('Parallel Google Analytics'),
    ];
    $form['parallel_tracking']['pga4'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Analytics 4 Account'),
      '#description' => $this->t("Enter your site's Google Analytics 4 <em>Measurement ID</em>."),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.pga4',
    ];
    $form['parallel_tracking']['parallelcd'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable sending the following custom dimensions to the parallel account'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.parallelcd',
    ];
    $form['parallel_tracking']['palagencydim'] = [
      '#type' => 'number',
      '#title' => $this->t('Agency Custom Dimension Slot #'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.palagencydim',
    ] + self::CUSTOM_DIMENSION_SLOT_SETTINGS;
    $form['parallel_tracking']['palsubagencydim'] = [
      '#type' => 'number',
      '#title' => $this->t('Sub-agency Custom Dimension Slot #'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.palsubagencydim',
    ] + self::CUSTOM_DIMENSION_SLOT_SETTINGS;
    $form['parallel_tracking']['palversiondim'] = [
      '#type' => 'number',
      '#title' => $this->t('Code Version Custom Dimension Slot #'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.palversiondim',
    ] + self::CUSTOM_DIMENSION_SLOT_SETTINGS;
    $form['parallel_tracking']['paltopicdim'] = [
      '#type' => 'number',
      '#title' => $this->t('Site Topic Custom Dimension Slot #'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.paltopicdim',
    ] + self::CUSTOM_DIMENSION_SLOT_SETTINGS;
    $form['parallel_tracking']['palplatformdim'] = [
      '#type' => 'number',
      '#title' => $this->t('Site Platform Custom Dimension Slot #'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.palplatformdim',
    ] + self::CUSTOM_DIMENSION_SLOT_SETTINGS;
    $form['parallel_tracking']['palscriptsrcdim'] = [
      '#type' => 'number',
      '#title' => $this->t('Script Source Custom Dimension Slot #'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.palscriptsrcdim',
    ] + self::CUSTOM_DIMENSION_SLOT_SETTINGS;
    $form['parallel_tracking']['palurlprotocoldim'] = [
      '#type' => 'number',
      '#title' => $this->t('URL Protocol Custom Dimension Slot #'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.palurlprotocoldim',
    ] + self::CUSTOM_DIMENSION_SLOT_SETTINGS;
    $form['parallel_tracking']['palinteractiontypedim'] = [
      '#type' => 'number',
      '#title' => $this->t('Interaction Type Custom Dimension Slot #'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.palinteractiontypedim',
    ] + self::CUSTOM_DIMENSION_SLOT_SETTINGS;
    $form['dapdev'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable DEV mode'),
      '#description' => $this->t('Enabling will cause traffic to be reported to a TEST/DEV DAP environment.'),
      '#config_target' => 'usfedgov_google_analytics.settings:query_parameters.dapdev',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // The asset library definitions have to be cleared for the changes to take
    // effect because the settings are added to the libraries dynamically.
    $this->libraryDiscovery->clearCachedDefinitions();
  }

}
