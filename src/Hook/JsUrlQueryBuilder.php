<?php

namespace Drupal\usfedgov_google_analytics\Hook;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Asset\AttachedAssetsInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Language\LanguageInterface;

/**
 * Builds and appends the DAP query parameters to JavaScript URLs.
 */
class JsUrlQueryBuilder {

  /**
   * The default values for DAP JavaScript query parameters.
   */
  const DEFAULT_PARAMETERS = [
    'agency' => '',
    'subagency' => '',
    'sitetopic' => '',
    'siteplatform' => '',
    'sp' => NULL,
    'exts' => NULL,
    'yt' => FALSE,
    'htmlvideo' => TRUE,
    'ytm' => 25,
    'sdor' => '',
    'autotracker' => TRUE,
    'cto' => 24,
    'pga4' => NULL,
    'parallelcd' => FALSE,
    'palagencydim' => 1,
    'palsubagencydim' => 2,
    'palversiondim' => 3,
    'paltopicdim' => 4,
    'palplatformdim' => 5,
    'palscriptsrcdim' => 6,
    'palurlprotocoldim' => 7,
    'palinteractiontypedim' => 8,
    'dapdev' => FALSE,
  ];

  public function __construct(protected ConfigFactoryInterface $configFactory) {}

  /**
   * Implements hook_js_alter().
   *
   * Appends the DAP settings query string to the JavaScript URL.
   */
  #[Hook('js_alter')]
  public function appendQueryString(&$javascript, AttachedAssetsInterface $assets, LanguageInterface $language) {
    $query = $this->buildQuery();
    foreach (array_keys($javascript) as $script) {
      if (preg_match('/Universal-Federated-Analytics(-Min)?.js/', $script)) {
        $javascript[$script]['data'] = $javascript[$script]['data'] . '?' . $query;
      }
    }
  }

  /**
   * Builds the DAP JavaScript query string.
   */
  public function buildQuery() {
    $settings = $this->configFactory->get('usfedgov_google_analytics.settings');
    $parameters = $settings->get('query_parameters') ?? [];

    foreach ($parameters as $key => $param) {
      if ($parameters[$key] === self::DEFAULT_PARAMETERS[$key] || $param === '') {
        unset($parameters[$key]);
        continue;
      }
      // URL query builders format boolean values as 0 or 1. Rewrite them as
      // strings so they're output as true or false per the DAP specifications.
      if (is_bool($param)) {
        $parameters[$key] = $param ? 'true' : 'false';
      }
    }
    return UrlHelper::buildQuery($parameters);
  }

}
