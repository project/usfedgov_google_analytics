<?php

namespace Drupal\usfedgov_google_analytics\Hook;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Checks usfedgov_google_analytics runtime requirements.
 */
class RuntimeRequirements {
  use StringTranslationTrait;

  public function __construct(protected ConfigFactoryInterface $configFactory) {}

  /**
   * Implements hook_runtime_requirements().
   *
   * Display a warning on the status report if the agency setting is not
   * configured.
   */
  #[Hook('runtime_requirements')]
  public function checkAgencySetting() {
    $config = $this->configFactory->get('usfedgov_google_analytics.settings');
    $requirements = [];

    if (empty($config->get('query_parameters.agency'))) {
      $requirements['usfedgov_google_analytics'] = [
        'title' => $this->t('U.S. Federal Government Google Analytics'),
        'description' => $this->t('The agency setting is required to be configured in the <a href=":url">settings form</a> before the analytics JavaScript will be attached to pages.', [
          ':url' => Url::fromRoute('usfedgov_google_analytics.form')->toString(),
        ]),
        'severity' => REQUIREMENT_WARNING,
        'value' => $this->t('The agency is not configured'),
      ];
    }
    return $requirements;

  }

}
