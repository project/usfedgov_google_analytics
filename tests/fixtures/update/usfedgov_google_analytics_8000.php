<?php
// phpcs:ignoreFile
/**
 * @file
 * A database fixture for usfedgov_google_analytics schema version 8000.
 *
 * This file was generated by the Drupal 11.2-dev db-tools.php script.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();

// Load the extensions and enable the module.
$extensions = $connection->select('config')
  ->fields('config', ['data'])
  ->condition('collection', '')
  ->condition('name', 'core.extension')
  ->execute()
  ->fetchField();
$extensions = unserialize($extensions);
$extensions['module']['usfedgov_google_analytics'] = 0;

$connection->update('config')
  ->fields(['data' => serialize($extensions)])
  ->condition('collection', '')
  ->condition('name', 'core.extension')
  ->execute();

$connection->insert('config')
->fields([
  'collection',
  'name',
  'data',
])
->values([
  'collection' => '',
  'name' => 'usfedgov_google_analytics.settings',
  'data' => 'a:5:{s:5:"_core";a:1:{s:19:"default_config_hash";s:43:"JipN-KqZNJIto1F98hInuXRHfav0GfU5MeGjtjdUgvM";}s:34:"usfedgov_google_analytics__disable";b:0;s:40:"usfedgov_google_analytics__load_from_cdn";i:0;s:40:"usfedgov_google_analytics__load_minified";b:0;s:35:"usfedgov_google_analytics__settings";a:3:{s:6:"agency";s:4:"usda";s:9:"subagency";s:3:"ars";s:5:"extra";a:21:{s:2:"sp";s:19:"my_search_parameter";s:4:"exts";s:4:"tiff";s:2:"yt";b:0;s:4:"sdor";s:1:"1";s:6:"dclink";s:1:"0";s:3:"aip";s:1:"1";s:3:"pua";s:0:"";s:4:"pga4";s:12:"G-1234567890";s:7:"enhlink";s:1:"0";s:11:"autotracker";s:1:"1";s:8:"forcessl";s:1:"0";s:6:"optout";s:1:"0";s:12:"fedagencydim";s:0:"";s:15:"fedsubagencydim";s:0:"";s:13:"fedversiondim";s:0:"";s:12:"palagencydim";s:12:"sample_dim_1";s:15:"palsubagencydim";s:2:"42";s:13:"palversiondim";s:4:"73.5";s:6:"maincd";s:1:"1";s:10:"parallelcd";s:1:"1";s:3:"cto";s:2:"12";}}}',
])
->execute();

$connection->insert('key_value')
->fields([
  'collection',
  'name',
  'value',
])
->values([
  'collection' => 'system.schema',
  'name' => 'usfedgov_google_analytics',
  'value' => 'i:8000;',
])
->execute();
