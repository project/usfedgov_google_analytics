<?php

namespace Drupal\Tests\usfedgov_google_analytics\Unit;

use Drupal\Core\Asset\AttachedAssetsInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Language\LanguageInterface;
use Drupal\usfedgov_google_analytics\Hook\JsUrlQueryBuilder;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Yaml\Yaml;

/**
 * Tests building the DAP JavaScript URL query string.
 *
 * @coversDefaultClass \Drupal\usfedgov_google_analytics\Hook\JsUrlQueryBuilder
 * @group usfedgov_google_analytics
 */
class JsUrlQueryBuilderTest extends UnitTestCase {

  /**
   * A mock config.factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A mock usfedgov_google_analytics.settings configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * An instance of the JsUrlQueryBuilder.
   *
   * @var \Drupal\usfedgov_google_analytics\Hook\JsUrlQueryBuilder
   */
  protected $queryBuilder;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->config = $this->createMock(ImmutableConfig::class);
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
    $this->configFactory->expects($this->once())
      ->method('get')
      ->with('usfedgov_google_analytics.settings')
      ->willReturn($this->config);
    $this->queryBuilder = new JsUrlQueryBuilder($this->configFactory);
  }

  /**
   * @covers ::appendQueryString
   */
  public function testAppendQueryString(): void {
    // Only bother to test for parameters you expect to be appended. Other tests
    // will check for excluding parameters.
    $parameters = [
      'agency' => 'usda',
      'subagency' => 'ars',
      'yt' => TRUE,
    ];
    $this->config->expects($this->once())
      ->method('get')
      ->with('query_parameters')
      ->willReturn($parameters);

    $javascript['Universal-Federated-Analytics.js']['data'] = 'Universal-Federated-Analytics.js';
    $javascript['Universal-Federated-Analytics-Min.js']['data'] = 'Universal-Federated-Analytics-Min.js';
    $assets = $this->createMock(AttachedAssetsInterface::class);
    $language = $this->createMock(LanguageInterface::class);
    $this->queryBuilder->appendQueryString($javascript, $assets, $language);

    // Check for individual parameters in the string rather than the string as a
    // whole. Otherwise, the test is dependent on a specific parameter order
    // which can change at any time.
    foreach ($parameters as $name => $value) {
      $parameter = $this->getExpectedParameter($name, $value);
      foreach ($javascript as $script) {
        $this->assertStringContainsString($parameter, $script['data']);
      }
    }
  }

  /**
   * Tests that individual parameters are correctly added to the query string.
   *
   * @param string $parameter
   *   The parameter to add to the query string.
   * @param string $type
   *   The parameter's schema type.
   *
   * @dataProvider providerQueryParameters
   */
  public function testIndividualParameters(string $parameter, string $type): void {
    // Verify that a default value has been added.
    $this->assertArrayHasKey($parameter, JsUrlQueryBuilder::DEFAULT_PARAMETERS, "A default value for '$parameter' must be added to JsUrlQueryBuilder::DEFAULT_PARAMETERS.");
    $default_value = JsUrlQueryBuilder::DEFAULT_PARAMETERS[$parameter];

    // Generate a test value for the parameter that will be added to the query.
    switch ($type) {
      case 'boolean':
        $value = !$default_value;
        break;

      case 'integer':
        $value = $default_value + 1;
        break;

      case 'string':
        $value = $this->getRandomGenerator()->word(4);
        break;

      default:
        $this->fail('JsUrlQueryBuilderTest::testIndividualParameters() was not set up to test parameters of this schema type. The test must be expanded to test this type.');
    }

    $this->config->expects($this->once())
      ->method('get')
      ->with('query_parameters')
      ->willReturn([
        $parameter => $value,
      ]);
    $query = $this->queryBuilder->buildQuery();

    $expected = $this->getExpectedParameter($parameter, $value);
    $this->assertEquals($expected, $query);
  }

  /**
   * Tests that default parameters are not added to the query string.
   *
   * @param string $parameter
   *   The parameter to add to the query string.
   *
   * @dataProvider providerQueryParameters
   */
  public function testDefaultParameters(string $parameter): void {
    // Verify that a default value has been added.
    $this->assertArrayHasKey($parameter, JsUrlQueryBuilder::DEFAULT_PARAMETERS, "A default value for '$parameter' must be added to JsUrlQueryBuilder::DEFAULT_PARAMETERS.");
    $default_value = JsUrlQueryBuilder::DEFAULT_PARAMETERS[$parameter];

    $this->config->expects($this->once())
      ->method('get')
      ->with('query_parameters')
      ->willReturn([
        $parameter => $default_value,
      ]);
    $query = $this->queryBuilder->buildQuery();
    $this->assertEquals('', $query);
  }

  /**
   * Tests that empty string parameters are not added to the query string.
   *
   * @param string $parameter
   *   The parameter to add to the query string.
   *
   * @dataProvider providerStringQueryParameters
   */
  public function testEmptyStringParameters(string $parameter): void {
    $this->config->expects($this->once())
      ->method('get')
      ->with('query_parameters')
      ->willReturn([
        $parameter => '',
      ]);
    $query = $this->queryBuilder->buildQuery();
    $this->assertEquals('', $query);
  }

  /**
   * Data provider for page attachment tests.
   *
   * @return string[][]
   *   An array of arrays that contain a query parameter name and its type from
   *   the configuration schema.
   */
  public static function providerQueryParameters() {
    $func_params = [];
    $config = Yaml::parseFile(__DIR__ . '/../../../config/schema/usfedgov_google_analytics.schema.yml');
    foreach ($config['usfedgov_google_analytics.settings']['mapping']['query_parameters']['mapping'] as $parameter => $schema) {
      $func_params[] = [$parameter, $schema['type']];
    }
    return $func_params;
  }

  /**
   * Data provider for string-specific page attachment tests.
   *
   * @return string[][]
   *   An array of arrays that contain a query parameter name and its type from
   *   the configuration schema.
   */
  public static function providerStringQueryParameters() {
    $parameters = self::providerQueryParameters();
    foreach ($parameters as $key => $data) {
      if ($data[1] != 'string') {
        unset($parameters[$key]);
      }
    }
    return $parameters;
  }

  /**
   * Builds an expected query string parameter.
   *
   * @param string $name
   *   The parameter name.
   * @param mixed $value
   *   The parameter value.
   *
   * @return string
   *   The built parameter.
   */
  protected function getExpectedParameter(string $name, $value): string {
    // Account for the string conversion of boolean values.
    if (is_bool($value)) {
      $value = $value ? 'true' : 'false';
    }
    return "$name=$value";
  }

}
