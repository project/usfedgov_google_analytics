<?php

namespace Drupal\Tests\usfedgov_google_analytics\Unit;

use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\usfedgov_google_analytics\Hook\PageAttachments;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Yaml\Yaml;

/**
 * Tests attaching the DAP JavaScript to pages.
 *
 * @coversDefaultClass \Drupal\usfedgov_google_analytics\Hook\PageAttachments
 * @group usfedgov_google_analytics
 */
class PageAttachmentsTest extends UnitTestCase {

  /**
   * Builds a mock config.factory service that will allow page attachment.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The mock config.factory service.
   */
  protected function validConfigFactory() {
    return $this->getConfigFactoryStub([
      'usfedgov_google_analytics.settings' => [
        'status' => TRUE,
        'library' => 'cdn',
        'query_parameters.agency' => 'usda',
      ],
    ]);
  }

  /**
   * Builds a mock current_user service that will allow page attachment.
   *
   * @return \Drupal\Core\Session\AccountProxy
   *   The mock current_user service.
   *
   * @throws \PHPUnit\Framework\MockObject\Exception
   */
  protected function validCurrentUser() {
    $current_user = $this->createMock(AccountProxy::class);
    $current_user->expects($this->any())
      ->method('isAuthenticated')
      ->willReturn(FALSE);
    return $current_user;
  }

  /**
   * Builds a mock current_route_match service that will allow page attachment.
   *
   * @return \Drupal\Core\Routing\RouteMatchInterface
   *   The mock current_route_match service.
   *
   * @throws \PHPUnit\Framework\MockObject\Exception
   */
  protected function validRouteMatch() {
    $route_match = $this->createMock(RouteMatchInterface::class);
    $route_match->expects($this->any())
      ->method('getRouteName')
      ->willReturn('<front>');
    return $route_match;
  }

  /**
   * Builds a mock router.admin_context service that will allow page attachment.
   *
   * @return \Drupal\Core\Routing\AdminContext
   *   The mock router.admin_context service.
   *
   * @throws \PHPUnit\Framework\MockObject\Exception
   */
  protected function validAdminContext() {
    $admin_context = $this->createMock(AdminContext::class);
    $admin_context->expects($this->any())
      ->method('isAdminRoute')
      ->willReturn(FALSE);
    return $admin_context;
  }

  /**
   * Tests successful attachment of libraries.
   *
   * @param string $library_name
   *   The machine name of the library to attach, without the
   *   'usfedgov_google_analytics.' prefix as the value is stored in
   *   usfedgov_google_analytics.settings.
   *
   * @dataProvider providerLibraryAttachment
   */
  public function testLibraryAttachment(string $library_name): void {
    $service = new PageAttachments(
      $this->getConfigFactoryStub([
        'usfedgov_google_analytics.settings' => [
          'status' => TRUE,
          'library' => $library_name,
          'query_parameters.agency' => 'usda',
        ],
      ]),
      $this->validCurrentUser(),
      $this->validRouteMatch(),
      $this->validAdminContext(),
    );
    $page = [];
    $result = $service->attachJavascript($page);
    $this->assertTrue($result);
    $this->assertEquals([
      '#attached' => ['library' => ['usfedgov_google_analytics/usfedgov_google_analytics.' . $library_name]],
      '#cache' => ['tags' => ['config:usfedgov_google_analytics.settings']],
    ], $page);
  }

  /**
   * Data provider for testLibraryAttachment().
   *
   * @return string[][]
   *   An array of arrays containing parameters for testLibraryAttachment().
   */
  public static function providerLibraryAttachment(): array {
    $library_file_contents = file_get_contents(__DIR__ . '/../../../usfedgov_google_analytics.libraries.yml');
    $libraries = Yaml::parse($library_file_contents);
    return array_map(function ($library) {
      return [substr($library, 4)];
    }, array_keys($libraries));
  }

  /**
   * Tests failure due to status being false.
   */
  public function testFalseStatus(): void {
    $service = new PageAttachments(
      $this->getConfigFactoryStub([
        'usfedgov_google_analytics.settings' => [
          'status' => FALSE,
          'library' => 'cdn',
          'query_parameters.agency' => 'usda',
        ],
      ]),
      $this->validCurrentUser(),
      $this->validRouteMatch(),
      $this->validAdminContext(),
    );
    $this->assertFailedAttachment($service);
  }

  /**
   * Tests failure due to a blank agency.
   */
  public function testBlankAgency(): void {
    $service = new PageAttachments(
      $this->getConfigFactoryStub([
        'usfedgov_google_analytics.settings' => [
          'status' => TRUE,
          'library' => 'cdn',
          'query_parameters.agency' => '',
        ],
      ]),
      $this->validCurrentUser(),
      $this->validRouteMatch(),
      $this->validAdminContext(),
    );
    $this->assertFailedAttachment($service);
  }

  /**
   * Tests failure due to an authenticated user.
   */
  public function testAuthenticatedUser(): void {
    $current_user = $this->createMock(AccountProxy::class);
    $current_user->expects($this->once())
      ->method('isAuthenticated')
      ->willReturn(TRUE);
    $service = new PageAttachments(
      $this->validConfigFactory(),
      $current_user,
      $this->validRouteMatch(),
      $this->validAdminContext(),
    );
    $this->assertFailedAttachment($service);
  }

  /**
   * Tests failure due to invalid routes.
   *
   * @param string $route_name
   *   The machine name of an invalid route.
   *
   * @dataProvider providerInvalidRoutes
   */
  public function testInvalidRoutes(string $route_name): void {
    $route_match = $this->createMock(RouteMatchInterface::class);
    $route_match->expects($this->once())
      ->method('getRouteName')
      ->willReturn($route_name);
    $service = new PageAttachments(
      $this->validConfigFactory(),
      $this->validCurrentUser(),
      $route_match,
      $this->validAdminContext(),
    );
    $this->assertFailedAttachment($service);
  }

  /**
   * Data provider for testInvalidRoutes().
   *
   * @return string[][]
   *   An array of arrays containing parameters for testInvalidRoutes().
   */
  public static function providerInvalidRoutes() {
    return array_map(function ($route_name) {
      return [$route_name];
    }, PageAttachments::INVALID_ROUTES);
  }

  /**
   * Tests failure due to an admin route.
   */
  public function testAdminRoute(): void {
    $admin_context = $this->createMock(AdminContext::class);
    $admin_context->expects($this->once())
      ->method('isAdminRoute')
      ->willReturn(TRUE);
    $service = new PageAttachments(
      $this->validConfigFactory(),
      $this->validCurrentUser(),
      $this->validRouteMatch(),
      $admin_context,
    );
    $this->assertFailedAttachment($service);
  }

  /**
   * Asserts that the DAP library was not attached by the service.
   *
   * @param \Drupal\usfedgov_google_analytics\Hook\PageAttachments $service
   *   The Drupal\usfedgov_google_analytics\Hook\PageAttachments service.
   */
  protected function assertFailedAttachment(PageAttachments $service) {
    $page = [];
    $result = $service->attachJavascript($page);
    $this->assertFalse($result);
    $this->assertEquals([], $page);
  }

}
