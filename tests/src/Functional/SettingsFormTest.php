<?php

namespace Drupal\Tests\usfedgov_google_analytics\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Checks the functionality of SettingsForm.
 *
 * @group usfedgov_google_analytics
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * A user with permission to administer settings.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['usfedgov_google_analytics'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer federal google analytics',
    ]);
  }

  /**
   * Tests various aspects of the settings form.
   */
  public function testSettingsForm(): void {
    $url = Url::fromRoute('usfedgov_google_analytics.form');

    // Verify that access permissions are set correctly.
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);

    // Verify that the form can be submitted without error.
    $this->submitForm(['agency' => 'usda'], 'Save configuration');
    $this->assertSession()->responseContains('The configuration options have been saved.');
  }

}
