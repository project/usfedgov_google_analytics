<?php

namespace Drupal\Tests\usfedgov_google_analytics\Functional\Update;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Tests setting updates and deletions.
 *
 * @group Update
 * @group usfedgov_google_analytics
 */
class UpdateSettingsTest extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {
    $this->databaseDumpFiles = [
      $this->root . '/core/modules/system/tests/fixtures/update/drupal-10.3.0.filled.standard.php.gz',
      __DIR__ . '/../../../fixtures/update/usfedgov_google_analytics_8000.php',
    ];
  }

  /**
   * @covers usfedgov_google_analytics_update_20002
   * @covers usfedgov_google_analytics_update_20003
   * @covers usfedgov_google_analytics_update_30000
   */
  public function testUpdateHookN(): void {
    $old_settings = \Drupal::config('usfedgov_google_analytics.settings');

    $this->assertFalse($old_settings->get('usfedgov_google_analytics__disable'));
    $this->assertSame(0, $old_settings->get('usfedgov_google_analytics__load_from_cdn'));
    $this->assertFalse($old_settings->get('usfedgov_google_analytics__load_minified'));

    $old_extra = $old_settings->get('usfedgov_google_analytics__settings.extra');
    $this->assertSame('1', $old_extra['sdor']);
    $this->assertSame('1', $old_extra['autotracker']);
    $this->assertSame('1', $old_extra['parallelcd']);
    $this->assertSame('12', $old_extra['cto']);

    $this->runUpdates();

    $new_settings = \Drupal::config('usfedgov_google_analytics.settings');

    $this->assertTrue($new_settings->get('status'));
    $this->assertEquals('local.8.0.0', $new_settings->get('library'));
    $this->assertNull($new_settings->get('usfedgov_google_analytics__load_from_cdn'));
    $this->assertNull($new_settings->get('usfedgov_google_analytics__load_minified'));

    // Ensure migrated query parameters exist and have the correct type.
    $parameters = $new_settings->get('query_parameters');
    $this->assertEquals('usda', $parameters['agency']);
    $this->assertEquals('ars', $parameters['subagency']);
    $this->assertSame('', $parameters['sitetopic']);
    $this->assertEquals('Drupal', $parameters['siteplatform']);
    $this->assertEquals('my_search_parameter', $parameters['sp']);
    $this->assertEquals('tiff', $parameters['exts']);
    $this->assertFalse($parameters['yt']);
    $this->assertTrue($parameters['htmlvideo']);
    $this->assertEquals(25, $parameters['ytm']);
    $this->assertSame('', $parameters['sdor']);
    $this->assertTrue($parameters['autotracker']);
    $this->assertEquals(12, $parameters['cto']);
    $this->assertEquals('G-1234567890', $parameters['pga4']);
    $this->assertTrue($parameters['parallelcd']);
    $this->assertEquals(1, $parameters['palagencydim']);
    $this->assertEquals(42, $parameters['palsubagencydim']);
    $this->assertEquals(73, $parameters['palversiondim']);
    $this->assertEquals(4, $parameters['paltopicdim']);
    $this->assertEquals(5, $parameters['palplatformdim']);
    $this->assertEquals(6, $parameters['palscriptsrcdim']);
    $this->assertEquals(7, $parameters['palurlprotocoldim']);
    $this->assertEquals(8, $parameters['palinteractiontypedim']);
    $this->assertFalse($parameters['dapdev']);
    $this->assertNull($new_settings->get('usfedgov_google_analytics__settings.extra'));
  }

}
