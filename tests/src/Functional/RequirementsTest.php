<?php

declare(strict_types=1);

namespace Drupal\Tests\usfedgov_google_analytics\Kernel;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the hook_requirements() implementation.
 *
 * @group usfedgov_google_analytics
 */
final class RequirementsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['usfedgov_google_analytics'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    require_once __DIR__ . '/../../../usfedgov_google_analytics.install';

  }

  /**
   * @covers usfedgov_google_analytics_requirements
   */
  public function testRequirements(): void {
    $config = \Drupal::configFactory()->getEditable('usfedgov_google_analytics.settings');
    $this->assertEmpty($config->get('query_parameters.agency'));

    // Ensure nothing is returned for the unused phases.
    $this->assertEquals([], usfedgov_google_analytics_requirements('install'));
    $this->assertEquals([], usfedgov_google_analytics_requirements('update'));

    $requirements = usfedgov_google_analytics_requirements('runtime')['usfedgov_google_analytics'];
    $this->assertEquals(REQUIREMENT_WARNING, $requirements['severity']);
    $this->assertEquals('The agency is not configured', $requirements['value']);

    $config->set('query_parameters.agency', 'usda');
    $config->save();

    $this->assertEquals([], usfedgov_google_analytics_requirements('runtime'));
  }

}
